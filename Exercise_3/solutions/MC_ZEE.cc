// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/ZFinder.hh"

namespace Rivet {


  /// @brief Add a short analysis description here
  class MC_ZEE : public Analysis {
  public:

    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(MC_ZEE);


    /// @name Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init() {

      // Initialise and register projections
      const FinalState fs;
      declare(FinalState(Cuts::abseta < 5 && Cuts::pT > 100*MeV), "fs");

      
      
      Cut cut = Cuts::abseta < 3.5 && Cuts::pT > 25*GeV;
      ZFinder zfinder(fs, cut,PID::ELECTRON, 65.0*GeV, 115.0*GeV,0.2,ZFinder::CLUSTERNODECAY, ZFinder::TRACK);
      declare(zfinder, "ZFinder");

      // Book histograms
      _h1["ZMass"] = bookHisto1D("ZMass", 20, 0.0, 200.0);

    }


    /// Perform the per-event analysis
    void analyze(const Event& event) {

      const double weight = event.weight();

      const ZFinder& zfinder = apply<ZFinder>(event, "ZFinder");
      /// @todo Do the event by event analysis here
      if (zfinder.bosons().size() != 1) vetoEvent;

      FourMomentum zmom(zfinder.bosons()[0].momentum());
      _h1["ZMass"]->fill(zmom.mass()/GeV, weight);
      
      
    }


    /// Normalise histograms etc., after the run
    void finalize() {

      const double sf(crossSection() / sumOfWeights());
      for (HistoMap1D::value_type& hist : _h1) { scale(hist.second, sf); }

      

    }

    //@}

  private:
    typedef map<string, Histo1DPtr> HistoMap1D;
    HistoMap1D _h1;

  };

  

  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(MC_ZEE);


}
